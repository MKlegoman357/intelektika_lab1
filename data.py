import csv

from stats import StatsAttribute

# Date,
# Location,
# MinTemp,
# MaxTemp,
# Rainfall,
# Evaporation,
# Sunshine,
# WindGustDir,
# WindGustSpeed,
# WindDir9am, WindDir3pm,
# WindSpeed9am, WindSpeed3pm,
# Humidity9am, Humidity3pm,
# Pressure9am, Pressure3pm,
# Cloud9am, Cloud3pm,
# Temp9am, Temp3pm,
# RainToday,
# RISK_MM,
# RainTomorrow

weather_attributes = [
    StatsAttribute("date", "Date", type="date", unique=True),
    StatsAttribute("location", "Location", type="category"),
    StatsAttribute("min_temp", "MinTemp", type="number"),
    StatsAttribute("max_temp", "MaxTemp", type="number"),
    StatsAttribute("rainfall", "Rainfall", type="number"),
    StatsAttribute("evaporation", "Evaporation", type="number", ignore=True),
    StatsAttribute("sunshine", "Sunshine", type="number", ignore=True),
    StatsAttribute("wind_gust_dir", "WindGustDir", type="category"),
    StatsAttribute("wind_gust_speed", "WindGustSpeed", type="number"),
    StatsAttribute("wind_dir_9am", "WindDir9am", type="category"),
    StatsAttribute("wind_dir_3pm", "WindDir3pm", type="category"),
    StatsAttribute("wind_speed_9am", "WindSpeed9am", type="number"),
    StatsAttribute("wind_speed_3pm", "WindSpeed3pm", type="number"),
    StatsAttribute("humidity_9am", "Humidity9am", type="number"),
    StatsAttribute("humidity_3pm", "Humidity3pm", type="number"),
    StatsAttribute("pressure_9am", "Pressure9am", type="number"),
    StatsAttribute("pressure_3pm", "Pressure3pm", type="number"),
    StatsAttribute("cloud_9am", "Cloud9am", type="number", ignore=True),
    StatsAttribute("cloud_3pm", "Cloud3pm", type="number", ignore=True),
    StatsAttribute("temp_9am", "Temp9am", type="number"),
    StatsAttribute("temp_3pm", "Temp3pm", type="number"),
    StatsAttribute("rain_today", "RainToday", type="category"),
    StatsAttribute("RISK_MM", "RISK_MM", type="number"),
    StatsAttribute("rain_tomorrow", "RainTomorrow", type="category"),
]

weather_attributes_dict = {attr.key: attr for attr in weather_attributes}


class WeatherRow:
    def __init__(self, data):
        self.raw_data = data
        self.date = data["Date"]
        self.location = data["Location"]
        self.min_temp = data["MinTemp"]
        self.max_temp = data["MaxTemp"]
        self.rainfall = data["Rainfall"]
        self.evaporation = data["Evaporation"]
        self.sunshine = data["Sunshine"]
        self.wind_gust_dir = data["WindGustDir"]
        self.wind_gust_speed = data["WindGustSpeed"]
        self.wind_dir_9am = data["WindDir9am"]
        self.wind_dir_3pm = data["WindDir3pm"]
        self.wind_speed_9am = data["WindSpeed9am"]
        self.wind_speed_3pm = data["WindSpeed3pm"]
        self.humidity_9am = data["Humidity9am"]
        self.humidity_3pm = data["Humidity3pm"]
        self.pressure_9am = data["Pressure9am"]
        self.pressure_3pm = data["Pressure3pm"]
        self.cloud_9am = data["Cloud9am"]
        self.cloud_3pm = data["Cloud3pm"]
        self.temp_9am = data["Temp9am"]
        self.temp_3pm = data["Temp3pm"]
        self.rain_today = data["RainToday"]
        self.RISK_MM = data["RISK_MM"]
        self.rain_tomorrow = data["RainTomorrow"]

        for attr in weather_attributes:
            value = getattr(self, attr.key)
            if value == "NA":
                setattr(self, attr.key, None)
            elif attr.type == "number":
                setattr(self, attr.key, float(value))
            elif attr.type == "category":
                setattr(self, attr.key, str(value))

    def __str__(self):
        return f"Date={self.date}," \
               f"Location={self.location}," \
               f"MinTemp={self.min_temp}," \
               f"MaxTemp={self.max_temp}," \
               f"Rainfall={self.rainfall}," \
               f"Evaporation={self.evaporation}," \
               f"Sunshine={self.sunshine}," \
               f"WindGustDir={self.wind_gust_dir}," \
               f"WindGustSpeed={self.wind_gust_speed}," \
               f"WindDir9am={self.wind_dir_9am},WindDir3pm={self.wind_dir_3pm}," \
               f"WindSpeed9am={self.wind_speed_9am},WindSpeed3pm={self.wind_speed_3pm}," \
               f"Humidity9am={self.humidity_9am},Humidity3pm={self.humidity_3pm}," \
               f"Pressure9am={self.pressure_9am},Pressure3pm={self.pressure_3pm}," \
               f"Cloud9am={self.cloud_9am},Cloud3pm={self.cloud_3pm}," \
               f"Temp9am={self.temp_9am},Temp3pm={self.temp_3pm}," \
               f"RainToday={self.rain_today}," \
               f"RISK_MM={self.RISK_MM}," \
               f"RainTomorrow={self.rain_tomorrow}"


def read_weather_data(path):
    data_list = []
    with open(path, newline="") as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            data_list.append(WeatherRow(row))
    return data_list


def write_weather_data(path, data):
    with open(path, "w", newline="") as file:
        header_row = [attr.name for attr in weather_attributes]
        csv_writer = csv.DictWriter(file, header_row)
        csv_writer.writeheader()
        for row in data:
            csv_writer.writerow({attr.name: getattr(row, attr.key) if getattr(row, attr.key) is not None else "NA"
                                 for attr in weather_attributes})
