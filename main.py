from tabulate import tabulate
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

import data
import stats


def print_file_stats(data_file_name, max_data=None):
    weather_data = data.read_weather_data(data_file_name)
    print(f"File: {data_file_name}")
    return (weather_data, *print_data_stats(weather_data, max_data))


def print_data_stats(weather_data, max_data=None):
    number_stats = {attr.key: stats.NumberAttributeStats(attr.key) for attr in data.weather_attributes
                    if attr.type == "number"}
    category_stats = {attr.key: stats.CategoryAttributeStats(attr.key) for attr in data.weather_attributes
                      if attr.type == "category"}
    print(f"Total data: {len(weather_data)}")
    # max_data = 100000
    max_data = len(weather_data) if max_data is None else max_data
    print(f"Processing data: {max_data}")
    print_progress = True
    for i, row in enumerate(weather_data):
        if print_progress and i % int(max_data / 10) == 0:
            print(f"Stats: {i * 100 / max_data:.1f}% ({i + 1} of {max_data})")
        for attr in data.weather_attributes:
            if attr.type == "number":
                number_stats[attr.key].add_value(getattr(row, attr.key))
            if attr.type == "category":
                category_stats[attr.key].add_value(getattr(row, attr.key))
        if i >= max_data - 1:
            break
    print("Calculating number stats...")
    for number_stat in number_stats.values():
        number_stat.calculate_stats()
    print("Calculating category stats...")
    for category_stat in category_stats.values():
        category_stat.calculate_stats()

    print("number stats:")
    print(tabulate(
        [[
            stat.name, stat.count, stat.empty_percentage, stat.cardinality,
            stat.min, stat.max, stat.quartile_1, stat.quartile_3, stat.average, stat.median, stat.std_dev
        ] for stat in number_stats.values()],
        ["Attribute", "Row Count", "Missing values, %", "Cardinality",
         "Min", "Max", "1st quartile", "3rd quartile", "Average", "Median", "Std dev."],
        tablefmt="psql"
    ))
    print("category stats:")
    print(tabulate(
        [[
            stat.name, stat.count, stat.empty_percentage, stat.cardinality,
            stat.mode, stat.mode_count, stat.mode_percentage, stat.mode_2, stat.mode_2_count, stat.mode_2_percentage
        ] for stat in category_stats.values()],
        ["Attribute", "Row Count", "Missing values, %", "Cardinality",
         "Mode", "Mode count", "Mode, %", "2nd Mode", "2nd Mode count", "2nd Mode, %"],
        tablefmt="psql"
    ))
    return number_stats, category_stats


def filter_data_file(input_file_name, output_file_name,
                     filter_func=lambda row, attr: getattr(row, attr.key) is not None):
    weather_data = data.read_weather_data(input_file_name)
    filtered_weather_data = []
    for row in weather_data:
        keep = True
        for attr in data.weather_attributes:
            if not filter_func(row, attr):
                keep = False
                break
        if keep:
            filtered_weather_data.append(row)
    data.write_weather_data(output_file_name, filtered_weather_data)


def show_histograms(number_stats, category_stats, number_bins=20):
    for key, number_stat in number_stats.items():
        plt.figure()
        plt.title(data.weather_attributes_dict[key].name)
        plt.hist(number_stat.non_empty_values, number_bins)
    for key, category_stat in category_stats.items():
        plt.figure()
        plt.title(data.weather_attributes_dict[key].name)
        plt.hist(category_stat.non_empty_values)
    plt.show()


if __name__ == "__main__":
    # print_file_stats("weatherAUS.csv")
    # filter_data_file("weatherAUS.csv", "weatherAUS_filtered_all_empty.csv")
    # print_file_stats("weatherAUS_filtered_all_empty.csv")
    # filter_data_file("weatherAUS.csv", "weatherAUS_filtered_all_empty_ignored_attrs.csv",
    #                  lambda row, attr: attr.ignore or getattr(row, attr.key) is not None)
    # print_file_stats("weatherAUS_filtered_all_empty_ignored_attrs.csv")

    # noinspection PyTupleAssignmentBalance
    weather_data, number_stats, category_stats = print_file_stats("weatherAUS.csv")

    # show_histograms(number_stats, category_stats)
